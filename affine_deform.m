function new_img = affine_deform(img, xlen, ylen, inpts, outpts)
    a = 1;
    function m = compute_mat(ps, w)
        m = zeros(2, 2);
        for in = 1 : length(ps)
            p = ps(in, :);
            m = m + 1/ (norm(p) ^ 2) * (transpose(p) * w(in) * p);
        end
    end

    function A = computeA(v, ins, outs, pstar, all_ws)
        l = length(outs);
        A = zeros(1, l);
        m = compute_mat(ins, all_ws);
        for n = 1 : l
            p = ins(n, :);
            pcap = 1 / norm(p) * p;
            if det(m) > 0
                A(n) = (v - pstar) / m * all_ws(n) * transpose(pcap);
            end
            
        end 
    end

    function vp = f(v, ins, outs, alpha)
        all_ws = compute_ws(v, ins, alpha);
        [pst, qst] = compute_centroids(ins, outs, all_ws);
        A = computeA(v, ins, outs, pst, all_ws);
        vp = qst;
        for it = 1 : length(outs)
            q = outs(it, :);
            vp = vp + A(it) * 1 / norm(q) * q;
        end
    end  
    
    function res = forward_fill(img, inpts, outpts, a)
        res = zeros(ylen, xlen, 3, 'int16') - 1;
        for i = 1: xlen
            for j = 1: ylen
                newpos = f([i j], inpts, outpts, a);
                newj = floor(newpos(2));
                newi = floor(newpos(1));
                if (newi > 0) && (newi <= xlen) && (newj > 0) && (newj <= ylen)
                    res(newj, newi, 1) = img(j, i, 1);
                    res(newj, newi, 2) = img(j, i, 2);
                    res(newj, newi, 3) = img(j, i, 3);
                end
            end
        end
    end
    
    new_img = forward_fill(img, inpts, outpts, a);
end