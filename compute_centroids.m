function [ps, qs] = compute_centroids(pin, qin, ws)
    len = length(pin);
    sumw = 0;
    sump = 0;
    sumq = 0;
    
    for n = 1: len
        w = ws(n);
        sumw = sumw + w;
        sump = sump + w * pin(n, :);
        sumq = sumq + w * qin(n, :);
    end
    
    ps = 1 / sumw * sump;
    qs = 1 / sumw * sumq;
end