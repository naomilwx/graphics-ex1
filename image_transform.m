function res = image_transform(filename, inpts, outpts, method, outfile)
    %
    % inpts: Original pixel positions of handles
    % outpts: Final pixel positions of handles
    % methods: a - affine, s - similarity, r - rigid
    %
    interpolmeth = 'natural';
    img = imread(filename);
    
    xlen = length(img(1, :)) / 3;
    ylen = length(img(:, 1));
    
    if method == 'a'
        res = affine_deform(img, xlen, ylen, inpts, outpts);
    elseif method == 's'
        res = similarity_or_rigid_deform(img, xlen, ylen, inpts, outpts, method);
    elseif method == 'r'
        res = similarity_or_rigid_deform(img, xlen, ylen, inpts, outpts, method);
    end
    
    function im = image_interpolation(img, xlen, ylen, method)
        function [X, Y, V, XQ, YQ] = construct_samples(img, c)
            X = [];
            Y = [];
            V = [];
            XQ = [];
            YQ = [];
            for xv = 1 : xlen
                for yv = 1: ylen
                    v = img(yv, xv, c);
                    if v >= 0
                        X =  [X; xv];
                        Y = [Y; yv];
                        V = [V; double(v)];
                    else
                        XQ = [XQ; xv];
                        YQ = [YQ; yv];
                    end
                end
            end
        end
        
        im = zeros(ylen, xlen, 3, 'uint8');
        
        for col = 1:3
            [X, Y, V, XQ, YQ] = construct_samples(img, col);
            for it = 1 : length(X)
                im(Y(it), X(it), col) = V(it);
            end
            VQ = griddata(X, Y, V, XQ, YQ, method);
            for it = 1: length(VQ)
                im(YQ(it), XQ(it), col) = floor(VQ(it));
            end
        end  
    end

    res = image_interpolation(res, xlen, ylen, interpolmeth);
    
    
    imwrite(res, outfile);
end