function ws = compute_ws(v, ps, alpha)
    l = length(ps);
    ws = zeros(1, l);
    for i = 1 : l
        p = ps(i, :);
        ws(i) = 1 / (norm(p - v)) ^ (2 * alpha);
    end
end