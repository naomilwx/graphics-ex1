function new_img = similarity_or_rigid_deform(img, xlen, ylen, inpts, outpts, method)
    a = 1;
    function A = computeA(v, ins, pstar, all_ws)
        l = length(ins);
        A = zeros(l, 4);
        for n = 1: l
            res = computeAi(v, n, ins, pstar, all_ws);
            
            for it = 1: 4
                A(l, it) = res(it);
            end
        end
    end
    
    function Ai = computeAi(v, pos, ins, pstar, all_ws)
        p = ins(pos, :);
        pcap = 1 / norm(p) * p;
        Ai = all_ws(pos) * [pcap; -1 * orthovec(pcap)] * transpose([(v - pstar); -1 * orthovec(v - pstar)]);
    end

    function Am = accessA(Ait)
        Am = zeros(2, 2);
        for it = 1: 4
            Am(it) = Ait(it);
        end
    end

    function comp = orthovec(v)
        comp(1) = v(2) * -1;
        comp(2) = v(1);
    end

    function u = computeMu(all_w, ps)
        u = 0;
        for it = 1: length(ps)
            p = ps(it, :);
            pcap = 1 / norm(p) * p;
            u = u + all_w(it) * dot(pcap, pcap);
        end
    end

    function vp = f(v, ins, outs, alpha)
        all_ws = compute_ws(v, ins, alpha);
        [pst, qst] = compute_centroids(ins, outs, all_ws);
        vp = qst;
        mu = computeMu(all_ws, ins);
        for it = 1: length(outs)
           q = outs(it, :);
           qcap = 1 / norm(q) * q;
           vp  = vp + qcap *  computeAi(v, it, ins, pst, all_ws) / mu;
        end
    end

    function frig = computefrigid(v, ins, outs, pst, all_ws)
        A = computeA(v, ins, pst, all_ws);
        frig = 0;
        for it = 1: length(outs)
            q = outs(it, :);
            qcap = 1 / norm(q) * q;
            frig = frig + qcap * accessA(A(it, :));
        end
    end

    function vr = fr(v, ins, outs, alpha)
        all_ws = compute_ws(v, ins, alpha);
        [pst, qst] = compute_centroids(ins, outs, all_ws);
        frig = computefrigid(v, ins, outs, pst, all_ws);
        vr = norm(v - pst) * frig / norm(frig) + qst;    
    end

    new_img = zeros(ylen, xlen, 3, 'int16') - 1;
    
    for i = 1: xlen
        for j = 1: ylen
            if method == 'r'
                newpos = fr([i j], inpts, outpts, a);
            else
                newpos = f([i j], inpts, outpts, a);
            end
            
            newj = floor(newpos(2));
            newi = floor(newpos(1));
            if (newi > 0) && (newi <= xlen) && (newj > 0) && (newj <= ylen)
                new_img(newj, newi, 1) = img(j, i, 1);
                new_img(newj, newi, 2) = img(j, i, 2);
                new_img(newj, newi, 3) = img(j, i, 3);
            end
            
        end
    end
end