function m = smooth_mesh(filename, outfile, niter, sg)
    %
    % niter - number of times to run smoothing
    % sg - sigma value for kernel function phi
    %
    [V,F,UV,C,~] = readOFF(filename);
    N = per_vertex_normals(V,F);
    dims = 3;
    
    l = length(V);
    m = zeros(l, dims);
        
    function res = f(x, s, n)
        res = n * (x - s);
    end

    function p = phi(x, s)
        r = x - s;
        dist = norm(r);
        p = exp(-1 * (dist/sg) ^ 2);
    end

    function gp = grad_phi(x, s)
       gp = -2 / (sg ^ 2) * phi(x,s) * (x - s);
    end

    function [s, n] = retrieve_point(id, V, N)
        s = transpose(V(id,:));
        n = N(id,:);
    end

    function fnc = compute_f(x, neighbours, V, N)
        top = 0;
        bottom = 0;
        for ind = 1 : length(neighbours)
            neigh = neighbours(ind);
            [s, n] = retrieve_point(neigh, V, N);
            p = phi(x, s);
            top = top + p * f(x, s, n);
            bottom = bottom + p;
        end
        if bottom ~= 0
            fnc = top / bottom;
        else
            fnc = 0;
        end
    end

    function all_f = precompute_f(V, N)
        all_f = zeros(l, 1);
        for it = 1: l
            x = transpose(V(it, :));
            all_f(it) = compute_f(x, get_neighbours(x), V, N);
        end
    end
    
     function gf = grad_f(x, xid, neighbours, V, N)
         gf = zeros(dims, 1);
         sumphi = 0;
         sumn = zeros(dims, 1);
         sumsnd = zeros(dims, 1);
         
         for idx = 1: length(neighbours)
             neigh = neighbours(idx);
             [s, n] = retrieve_point(neigh, V, N);
             nt = transpose(n);
             
             p = phi(x, s);
             sumn = sumn + p * nt;
             sumsnd = sumsnd + grad_phi(x, s) * (f(x, s, n) - all_f(xid));
             sumphi = sumphi + p;
         end
         
         if sumphi ~= 0
             gf = (sumn + sumsnd) / sumphi;
         end
     end

    function neighbours = get_neighbours(vertex)
        srad = sg * 3;
        [neighbours dst numr] = frsearch(kdtree, vertex, srad, min(200, l - 1), 0, false);
    end

    function vp = smoothed_p(vertex, vid, V, N)
        x = transpose(V(vid, :));        
        vp = vertex - all_f(vid) * grad_f(x, vid, get_neighbours(x), V, N);
    end
    
    function [mat, factor, normV] = normaliseV(V)
        maxX = max(V(:, 1));
        minX = min(V(:, 1));
        maxY = max(V(:, 2));
        minY = min(V(:, 2));
        maxZ = max(V(:, 3));
        minZ = min(V(:, 3));
        
        ref = [minX minY minZ];
        factor = max([(maxX- minX) (maxY- minY) (maxZ- minZ)]);
        mat = repmat(ref, length(V), 1);
        normV = (V - mat) / factor;
    end
    
    [mat, factor, V] = normaliseV(V);
    
    for i = 1: l
        m(i, :) = V(i, :);
    end
    
    kdtree = ann(transpose(V));
    all_f = precompute_f(V, N);
    
    for iter = 1: niter
        for i = 1: l
            pt = transpose(m(i, :));
            m(i, :) = smoothed_p(pt, i, V, N);
        end
        close(kdtree);
        kdtree = ann(transpose(m));
        all_f = precompute_f(m, N);
    end
    
    m = m * factor + mat;
    
    writeOFF(outfile, m, F, UV, C, N);
end