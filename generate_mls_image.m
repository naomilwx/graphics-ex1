function generate_mls_image(xi, nix, yi, niy, sg)
    % 
    % xi - X coordinates of samples
    % nix - X coordinates of sample normals
    % yi - Y coordinates of samples
    % niy - Y coordinates of sample normals
    % sg - sigma value to use for kernel function, phi
    %
    
    set(gca,'YDir','normal');
    
    function all_c = mls_compute_and_display(xi, nix, yi, niy, sg)
        nump = length(xi);
        gridsize = 0.01;
        xpadding = 0;
        ypadding = 100;

        function r = magnitude_sq(x)
           r =  x(1)^2 + x(2)^2;
        end

        function res = f(x, s, n)
            res = n*(x - s);
        end

        function p = phi(x, s)
            p = exp(-1 * (magnitude_sq(x-s)/sg^2));
        end

        function [up, down, num] = compute_bounds(high, low, padding)
           up =  ceil(high) + gridsize * padding;
           down = floor(low) - gridsize * padding;
           num = floor((up - down)/gridsize);
        end

        [upx, downx, xnum] = compute_bounds(max(xi), min(xi), xpadding);
        [upy, downy, ynum] = compute_bounds(max(yi), min(yi), ypadding);

        function [cenx, ceny] = get_grid_center(idx, idy)
           cenx = downx + (idx - 0.5) * gridsize;
           ceny = downy + (idy - 0.5) * gridsize;
        end

        all_c = zeros(ynum, xnum);

        function c = compute_c(cenx, ceny)
            top = 0;
            bottom = 0;
            for id = 1: nump
                n = [nix(id) niy(id)];
                s = [xi(id); yi(id)];
                pt = [cenx; ceny];
                p = phi(pt, s);
                res = f(pt, s, n);
                top = top + p*res;
                bottom = bottom + p;
            end
            
            if bottom ~= 0
                c = top/bottom;
            else
                c = 0;
            end
        end

        for i = 1: xnum
            for j = 1: ynum
                [cenx, ceny] = get_grid_center(i, j);
                all_c(j, i) = compute_c(cenx, ceny); %deliberate, rows of the all_c correspond to the y axis...
            end
        end
        imagesc(downx:gridsize:upx, downy:gridsize:upy, all_c);
    end

    mls_compute_and_display(xi, nix, yi, niy, sg);
    hold on;
    plot(xi, yi);
    
end